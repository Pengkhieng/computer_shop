import React from "react";
import Header from "../components/Header"
import Button from "react-bootstrap/Button";
export default function Landing() {
  return (
    <div>
      <Header />
      <h1>
        <Button as="a" variant="primary">
          Button as link
        </Button>
      </h1>
    </div>
  );
}
